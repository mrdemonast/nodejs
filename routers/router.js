const express = require('express');
const Controller = require('../controllers/controller');

const router = express.Router();

router.post('/', Controller.createFile);
router.get('/', Controller.getAllFiles);
router.get('/:filename', Controller.getFile);
router.put('/', Controller.updateFile);
router.delete('/:filename', Controller.deleteFile)

module.exports = router;

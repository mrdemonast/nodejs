const fs = require('fs');
const path = require('path');
const logger = require('../helpers/logger');

class Controller {
  createFile(req, res) {
    try {
      const { filename, content } = req.body;
      const filesFolder = path.join(__dirname, '../', 'files');
      if(!fs.existsSync(filesFolder)){
        fs.mkdirSync(filesFolder)
      }
      const files = fs.readdirSync('./files/');
      
      if (!filename) {
        logger('POST', 400);
        return res
          .status(400)
          .send({ message: `Please specify 'filename' parameter` });
      }

      if (!content) {
        logger('POST', 400);
        return res
          .status(400)
          .send({ message: `Please specify 'content' parameter` });
      }

      if (!content && !filename) {
        logger('POST', 400);
        return res.status(400).send({ message: 'Please specify parameters' });
      }

      if (!/\.(txt|js|xml|yaml|log|json)$/g.test(filename)) {
        logger('POST', 400);
        return res
          .status(400)
          .send({ message: 'Please specify file extension' });
      }

      if (files.includes(filename)) {
        logger('POST', 400);
        return res
          .status(400)
          .send({ message: `File with name ${filename} is already exist` });
      }
      fs.createWriteStream('files/' + filename).write(content);
      logger('POST', 200);
      return res.status(200).send({ message: 'File created successfully' });
    } catch (error) {
      logger('POST', 500);
      return res.status(500).send({ message: error.message });
    }
  }

  getAllFiles(req, res) {
    try {
      const files = fs.readdirSync('./files')
      logger('GET', 200);
      if(!files) {
        logger('GET', 400);
        return res.status(400).send({ message: 'There are no files in File system'})
      }
      return res.status(200).send({ message: 'Success', files: files });
    } catch (error) {
      logger('GET', 500);
      return res.status(500).send({ message: error.message });
    }
  }

  getFile(req, res) {
    try {
      const uploadedDate = new Date(Date.now()).toISOString();
      if (fs.existsSync('files/' + req.params.filename)) {
        const content = fs.readFileSync(
          './files/' + req.params.filename,
          'utf8'
        );
        logger('GET', 200);
        return res.status(200).send({
          message: 'Success',
          filename: req.params.filename,
          content: content,
          extension: path.extname(req.params.filename).replace(/\./, ''),
          uploadedDate: uploadedDate,
        });
      } else {
        logger('GET', 400);
        return res.status(400).send({
          message: `No file with '${req.params.filename}' filename found`,
        });
      }
    } catch (error) {
      logger('GET', 500);
      return res.status(500).send({ message: error.message });
    }
  }
  updateFile(req, res) {
    try {
      if (fs.existsSync('files/' + req.body.filename)) {
        fs.writeFile(
          './files/' + req.body.filename,
          req.body.content,
          (error) => console.log(error)
        );
        logger('PUT', 200);
        return res.status(200).send({
          message: `File ${req.body.filename} updated successfully`,
        });
      } else {
        logger('PUT', 400);
        return res.status(400).send({
          message: `File with name ${req.body.filename} doesn't exist`,
        });
      }
    } catch (error) {
      logger('PUT', 500);
      return res.status(500).send({ message: error.message });
    }
  }

  deleteFile(req, res) {
    try {
      const { filename } = req.params;
      if (fs.existsSync('files/' + filename)) {
        fs.unlink('./files/' + filename, (error) => console.log(error));
        logger('DELETE', 200);
        return res.status(200).send({
          message: `File deleted successfully`,
        });
      } else {
        logger('DELETE', 400);
        return res.status(400).send({
          message: `File ${filename} is not found`,
        });
      }
    } catch (error) {
      logger('DELETE', 500);
      return res.status(500).send({ message: error.message });
    }
  }
}

module.exports = new Controller();

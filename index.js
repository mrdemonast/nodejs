const express = require('express');
const cors = require('cors');
const router = require('./routers/router');

const PORT = +process.env.PORT || 8080;
const app = express();

app.use(cors());
app.use(express.json());
app.use('/api/files/', router);

app.listen(PORT, () => {
  console.log('Listening on port: ', PORT);
});